// I detta kodstycke så körs all konfiguration
void setup() {                
  // Här säger vi åt arduinot att vi ska köra ström på port 12
  pinMode(12, OUTPUT);     
}

// Denna kod körs om och om igen
void loop() {
  digitalWrite(led, HIGH);   // Slå på strömmen på port 12
  delay(1000);               // Vänta i 1000 millisekunder (Eller en sekund)
  digitalWrite(led, LOW);    // Slå av strömmen på port 12
  delay(1000);               // Vänta igen
}
